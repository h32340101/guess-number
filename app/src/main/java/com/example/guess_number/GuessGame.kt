package com.example.guess_number

import java.util.*

class GuessGame {
    var secret = 0
    var times = 0
    var message = ""

    init {
        reset()
    }

    fun reset() {
        secret = Random().nextInt(10) + 1
        times = 0
    }

    fun guess(number: Int): Boolean {
        var isCorrect = false
        val diff = diff(number)
        times++
        message = when {
            (diff > 0) -> "smaller"
            (diff < 0) -> "bigger"
            else -> { isCorrect = true;  "got it" }
        }
        message += ", 第 $times 次"
        return isCorrect
    }

    private fun diff(number: Int): Int {
        return number - secret
    }
}