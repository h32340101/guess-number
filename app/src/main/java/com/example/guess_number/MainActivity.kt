package com.example.guess_number

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val sharedPreferences by lazy {
        getSharedPreferences("guess", Context.MODE_PRIVATE)
    }
    private val guessGame = GuessGame()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val times = sharedPreferences.getInt("times", 0)
        if (times != 0) {
            AlertDialog.Builder(this)
                .setTitle("Guess")
                .setMessage("繼續上次的進度")
                .setPositiveButton("yes") { _, _ ->
                    load()
                }
                .setNegativeButton("no", null)
                .show()
        }

        guessButton.setOnClickListener {
            val numberStr = numberEditText.text.toString()
            val number = numberStr.toIntOrNull()
                ?: run {
                    AlertDialog.Builder(this@MainActivity)
                        .setTitle("Guess")
                        .setMessage("請輸入數字")
                        .setNeutralButton("ok", null)
                        .show()
                    return@setOnClickListener
                }
            val isCorrect = guessGame.guess(number)
            val message = guessGame.message

            AlertDialog.Builder(this).apply {
                setTitle("Guess")
                setMessage(message)
                if (isCorrect) {
                    setPositiveButton("replay") { _, _ ->
                        reset()
                    }
                }
                setNeutralButton("ok", null)
            }.show()
        }

        replayButton.setOnClickListener {
            AlertDialog.Builder(this)
                .setTitle("Guess")
                .setMessage("是否重玩")
                .setPositiveButton("ok") { _, _ ->
                    reset()
                }
                .setNeutralButton("cancel", null)
                .show()
        }
    }

    override fun onStop() {
        super.onStop()
        sharedPreferences.edit()
            .putInt("secret", guessGame.secret)
            .putInt("times", guessGame.times)
            .putString("message", guessGame.message)
            .putString("number", numberEditText.text.toString())
            .apply()
    }

    private fun reset() {
        guessGame.reset()
        "第 0 次".also { timesTextView.text = it }
    }

    private fun load() {
        sharedPreferences.getInt("times", 0).also { guessGame.times = it }
        sharedPreferences.getInt("secret", 0).also { guessGame.secret = it }
        sharedPreferences.getString("message", null).also { guessGame.message = it ?: "" }
        sharedPreferences.getString("number", null).also { numberEditText.setText(it) }
        "第 ${guessGame.times} 次".also { timesTextView.text = it }
    }
}